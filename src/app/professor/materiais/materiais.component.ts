import { Component } from "@angular/core";

@Component({
  selector: "materiais",
  template: `<router-outlet></router-outlet>`,
})
export class MateriaisComponent { }
