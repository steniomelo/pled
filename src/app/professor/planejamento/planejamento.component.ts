import { Component } from "@angular/core";

@Component({
    selector: "planejamento",
    template: `<router-outlet></router-outlet>`,
})
export class PlanejamentoComponent { }
