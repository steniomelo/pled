import { NgModule } from "@angular/core";
import {
  NbCardModule,
  NbIconModule,
  NbInputModule,
  NbTreeGridModule,
  NbButtonModule,
  NbActionsModule,
  NbCheckboxModule,
  NbDatepickerModule,
  NbRadioModule,
  NbSelectModule,
  NbAccordionModule,
  NbUserModule,
  NbSpinnerModule,
  NbCalendarRangeModule,
} from "@nebular/theme";

import { NgxMaskModule } from 'ngx-mask';


import { ReactiveFormsModule } from '@angular/forms';


import { Ng2SmartTableModule } from "ng2-smart-table";

import { ThemeModule } from "../../@theme/theme.module";

import { TurmasRoutingModule, routedComponents } from './turmas-routing.module';


@NgModule({
  imports: [
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    NbActionsModule,
    NbCheckboxModule,
    NbDatepickerModule,
    NbRadioModule,
    NbSelectModule,
    NbUserModule,
    ThemeModule,
    NbButtonModule,
    TurmasRoutingModule,
    Ng2SmartTableModule,
    ReactiveFormsModule,
    NgxMaskModule,
    NbSpinnerModule,
    NbCalendarRangeModule,
    NbAccordionModule
  ],
  declarations: [...routedComponents],
})
export class TurmasModule { }
