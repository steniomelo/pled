import { Component } from "@angular/core";

@Component({
    selector: "professores",
    template: `<router-outlet></router-outlet>`,
})
export class ProfessoresComponent { }
