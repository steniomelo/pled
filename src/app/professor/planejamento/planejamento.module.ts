import { NgModule } from "@angular/core";
import {
  NbCardModule,
  NbIconModule,
  NbInputModule,
  NbTreeGridModule,
  NbButtonModule,
  NbActionsModule,
  NbCheckboxModule,
  NbDatepickerModule,
  NbRadioModule,
  NbSelectModule,
  NbUserModule,
  NbSpinnerModule,
  NbAccordionModule

} from "@nebular/theme";

import { NgxMaskModule } from 'ngx-mask';
import { ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from "ng2-smart-table";

import { FormsModule } from '@angular/forms';
import { FlatpickrModule } from 'angularx-flatpickr';

import { ThemeModule } from "../../@theme/theme.module";
import {
  PlanejamentoRoutingModule,
  routedComponents,
} from "./planejamento-routing.module";
import { ListarComponent } from './listar/listar.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { Ng2FlatpickrModule } from 'ng2-flatpickr';


@NgModule({
  imports: [
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    NbActionsModule,
    NbCheckboxModule,
    NbDatepickerModule,
    NbRadioModule,
    NbSelectModule,
    NbUserModule,
    ThemeModule,
    NbButtonModule,
    PlanejamentoRoutingModule,
    Ng2SmartTableModule,
    ReactiveFormsModule,
    NgxMaskModule,
    NbSpinnerModule,
    FormsModule,
    FlatpickrModule,
    Ng2FlatpickrModule,
    NbAccordionModule,
    DragDropModule
  ],
  declarations: [...routedComponents, ListarComponent],
})
export class PlanejamentoModule { }
