import { Component } from "@angular/core";

@Component({
  selector: "turmaas",
  template: `<router-outlet></router-outlet>`,
})
export class TurmasComponent { }
