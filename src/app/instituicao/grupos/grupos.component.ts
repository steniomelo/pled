import { Component } from "@angular/core";

@Component({
  selector: "grupos",
  template: `<router-outlet></router-outlet>`,
})
export class GruposComponent { }
