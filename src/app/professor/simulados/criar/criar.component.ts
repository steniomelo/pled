import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { ProfessorService } from "../../professor.service";
import { finalize } from 'rxjs/operators';
import { NbCalendarRange, NbDateService, } from '@nebular/theme';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';

@Component({
  selector: 'nb-select-clean',
  templateUrl: './criar.component.html',
  styleUrls: ['./criar.component.scss']
})

export class CriarComponent implements OnInit {

  form!: FormGroup;
  formSimuladoTurma!: FormGroup;
  isLoading: Boolean = true;
  turmas = [];
  turmasFiltered = [];
  disciplinas = [];
  public Editor = ClassicEditor;
  public ckeditorConfig = {
    toolbar: [
        'heading',
        '|',
        'bold',
        'italic',
        'link',
        'bulletedList',
        'numberedList',
        '|',
        'indent',
        'outdent',
        '|',
        'blockQuote',
        'insertTable',
        'mediaEmbed',
        'undo',
        'redo'
      ]
  }

  constructor(private formBuilder: FormBuilder, private ProfessorService: ProfessorService, protected router: Router) {

  }

  ngOnInit(): void {
    this.getTurmasProfessor();
    this.getDetalhesProfessor();
    this.createForm();

    this.form.get("disciplina").valueChanges.subscribe(selectedValue => {

      this.turmasFiltered = this.turmas.filter(
        turma => turma.disciplinas.some(disciplina => disciplina.idProfessorDisciplina == selectedValue
        ));

    })
  }

  onDrop(event: CdkDragDrop<any[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }

  get questoes() {
    return this.form.get('questoes') as FormArray;
  }

  getTurmasProfessor() {
    this.ProfessorService
      .getTurmas()
      .subscribe((response) => {
        this.isLoading = false;
        this.turmas = response;
      });
  }

  getDetalhesProfessor() {
    this.ProfessorService
      .getProfessorDetalhes()
      .subscribe((response) => {
        this.isLoading = false;
        this.disciplinas = response.disciplinas;
      });
  }

  addQuestoes() {
    this.questoes.push(this.formBuilder.group({
      enunciado: ['', Validators.required],
      pontos: ['', Validators.required],
      isMultiplaEscolha: [true, Validators.required],
      alternativas: this.formBuilder.array([]),
    }));
  }


  deleteQuestoes(index) {
    this.questoes.removeAt(index);
  }

  addAlternativa(control) {
    control.push(this.formBuilder.group({
      texto: ['', Validators.required],
      isResposta: [false, Validators.required]
    }));
  }


  deleteAlternativa(control, index) {
    control.removeAt(index);
  }

  filterDisciplinas(event) {
    console.log(event);
  }

  private createForm() {

    this.form = this.formBuilder.group({
      titulo: ['', Validators.required],
      descricao: ['', Validators.required],
      tipo: ['', Validators.required],
      questoes: this.formBuilder.array([]),
      prazoInicial: ['', Validators.required],
      prazoFinal: ['', Validators.required],
      turmas: ['', Validators.required],
      disciplina: ['', Validators.required]

    });


  }

  createFormSimuladoTurma() {
    this.formSimuladoTurma = this.formBuilder.group({
      idSimulado: ['', Validators.required],
      turmas: ['', Validators.required],
      disciplina: ['', Validators.required]
    });
  }

  Adicionar() {
    this.isLoading = true;
    const result: simulado = Object.assign({}, this.form.value);
    this.ProfessorService
      .criarSimulado(result)
      .pipe(finalize(() => { this.isLoading = false; }))
      .subscribe((response) => {

        this.isLoading = false;

        if (response) {
          this.inserirSimuladoTurma(response._id);

        }
      });
  }


  inserirSimuladoTurma(idSimulado) {
    this.isLoading = true;

    let dados = {
      idSimulado: idSimulado,
      turmas: this.form.controls['turmas'].value,
      idDisciplinaProfessor: this.form.controls['disciplina'].value
    }

    console.log(dados);
    this.ProfessorService
      .inserirSimuladoTurma(dados)
      .pipe(finalize(() => { this.isLoading = false; }))
      .subscribe((response) => {

        this.isLoading = false;

        if (response) {
          Swal.fire('Ok', 'Atividade adicionada com sucesso', 'success');
          this.router.navigateByUrl("/atividades");
        }
      });

  }
}

export class simulado {
  nome: string = '';
  descricao: string = '';
  titulo: string = '';
  questoes: any = [];
  prazoInicial: string = '';
  prazoFinal: string = '';
}