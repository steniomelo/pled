import { Component } from "@angular/core";

@Component({
  selector: "usuarios",
  template: `<router-outlet></router-outlet>`,
})
export class UsuariosComponent { }