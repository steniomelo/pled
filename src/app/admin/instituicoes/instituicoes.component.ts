import { Component } from "@angular/core";

@Component({
  selector: "instituicoes",
  template: `<router-outlet></router-outlet>`,
})
export class instituicoesComponent { }
