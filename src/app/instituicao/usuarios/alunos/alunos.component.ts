import { Component } from "@angular/core";

@Component({
  selector: "alunos",
  template: `<router-outlet></router-outlet>`,
})
export class AlunosComponent { }
