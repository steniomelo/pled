import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TurmasEditarComponent } from './editar.component';

describe('TurmasEditarComponent', () => {
  let component: TurmasEditarComponent;
  let fixture: ComponentFixture<TurmasEditarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TurmasEditarComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TurmasEditarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
