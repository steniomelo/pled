import { Component } from "@angular/core";

@Component({
  selector: "disciplinas",
  template: `<router-outlet></router-outlet>`,
})
export class DisciplinasComponent { }
